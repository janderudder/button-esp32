# Button

Button handling for Arduino platform.

- Handles debounce
- Actions: `Pressed`, `Released`, `LongPressed`, `Repeated`


## Usage

### Creation

Construct a button object. If you use the constructor without parameters, you
need to call the `begin()` method, passing at least the pin number, and optionally a pin mode. Otherwise you can set these parameters directly in the
constructor.

#### Button construction methods
```cpp
Button() = default;

Button(uint8_t pin, uint8_t pinMode =INPUT, uint8_t activeState =Auto);

void begin(uint8_t pin, uint8_t pinMode =INPUT, uint8_t activeState =Auto);
```

The active state is determined automatically: `HIGH` for `INPUT` mode, `LOW` for `INPUT_PULLUP` mode. You can also set that parameter manually in the constructor or `begin()`.

The button handles the call to `pinMode` itself.


### Reading

Call the `read()` method once per loop and button.

This method refreshes the state of the button. Then it can be interrogated with the corresponding methods, or you can compare the return
value of `read()` to an `enum` of type `Button::Action`.

#### Button interrogation methods
```cpp
enum Button::Action { None, Pressed, Released, LongPressed, Repeated };

Button::Action read(Option option = NoOption, t_millis interval = 1000);

bool isPressed() const;
bool isReleased() const;
bool isRepeated() const;
bool isLongPressed() const;
```

The `interval` parameter controls the delay of the optional action, which can be one of the following:

```cpp
enum Option { NoOption, HandleRepeat, HandleLongPress };
```

See `include/Button.hpp` for more documentation.


## Example

```cpp
#include "Button.hpp"

// GPIO 1, INPUT_PULLUP, active LOW (Auto)
Button button1(1, INPUT_PULLUP);

// GPIO 2, INPUT (default), active HIGH (Auto)
Button button2(2);

// we'll configure this one in the setup for example purposes
Button button3;

void setup()
{
    // configure button 3
    button3.begin(3, INPUT_PULLUP);

    // change the debounce delay for button 1, for example purposes
    button1.setDebounceDelay(80);

    Serial.begin(9600);
}

void loop()
{
    // Read the state of button 1.
    // This is mandatory to get an up to date answer from the various getters
    // like isPressed(), isReleased() etc.
    button1.read();

    // Let's test if button 1 was pressed or released since the last loop()
    // iteration
    if (button1.isPressed()) {
        Serial.println("Button 1 pressed");
    }
    else if (button1.isReleased()) {
        Serial.println("Button 1 released");
    }

    // For button 2 we also want to know if it's held down.
    // In that case we want to get a Repeated signal every 1000 milliseconds.
    // Here we're not using the getter methods but testing the action returned
    // by read, it can be done either way.
    switch (button2.read(Button::HandleRepeat, 1000))
    {
        case Button::Pressed:
            Serial.printf("Button 2 pressed at timestamp %lu\n", millis());
            break;

        case Button::Released:
            Serial.println("Button 2 released");
            break;

        case Button::Repeated:
            Serial.printf("Button 2 repeated at timestamp %lu\n", millis());

        default:
            break;
    }

    // On button 3 we want to react to a single long press of 2 seconds.
    if (button3.read(Button::HandleLongPress, 2000) == Button::LongPressed) {
        Serial.println("Button 3 was pressed for a while");
    }

    // We can mix the ways to interrogate the button, just don't call read()
    // more than once per button and per loop().
    if (button3.isPressed()) {
        Serial.println("Button 3 was just pressed");
    }
}
```
