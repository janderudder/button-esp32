#include "Button.hpp"


// module level functions
////////////////////////////////////////////////////////////////////////////////
static uint8_t _activeStateForPinMode(uint8_t pinMode)
{
    if (pinMode == INPUT_PULLUP) {
        return LOW;
    } else {
        return HIGH;
    }
}


// class variables
////////////////////////////////////////////////////////////////////////////////
Button::t_millis Button::mg_defaultDebounceDelay = 50;

uint8_t const Button::Auto = 2;

uint8_t const Button::Infinite = 0;

Button::t_millis const Button::NoRepeat = (Button::t_millis)(-1);


// class methods
////////////////////////////////////////////////////////////////////////////////
void Button::setDefaultDebounceDelay(t_millis_short delayMs)
{
    mg_defaultDebounceDelay = delayMs;
}


// instance methods
////////////////////////////////////////////////////////////////////////////////
Button::Button(uint8_t pin, uint8_t pin_mode, uint8_t activeState)
{
    begin(pin, pin_mode, activeState);
}


void Button::begin(uint8_t pin, uint8_t pin_mode, uint8_t activeState)
{
    m_activeState = (activeState==Button::Auto) ?
        _activeStateForPinMode(pin_mode) : activeState;

    m_pin = pin;
    m_debounceDelay = mg_defaultDebounceDelay;
    m_repeatCount = 0;

    pinMode(m_pin, pin_mode);
    delayMicroseconds(100);
    m_lastState = digitalRead(m_pin);
}


void Button::setDebounceDelay(t_millis_short delayMs)
{
    m_debounceDelay = delayMs;
}


////////////////////////////////////////////////////////////////////////////////
Button::Action Button::read(Option option, t_millis interval)
{
    m_action = None;
    t_millis const currentTime = millis();

    if (currentTime - m_lastStateTime >= m_debounceDelay)
    {
        bool const currentState = digitalRead(m_pin);

        // state change
        if (currentState != m_lastState)
        {
            m_lastState = currentState;
            m_lastStateTime = currentTime;
            m_lastRepeatTime = currentTime;
            if (currentState == m_activeState) {
                m_action = Pressed;
                m_repeatCount = 0;
            } else {
                m_action = Released;
            }
        }
        // state: no change and active
        else if (m_lastState == m_activeState
            && (currentTime - m_lastRepeatTime >= interval))
        {
            m_lastRepeatTime = currentTime;
            m_repeatCount += 1;
            if (option == HandleRepeat) {
                m_action = Repeated;
            }
            else if (option == HandleLongPress && m_repeatCount == 1) {
                m_action = LongPressed;
            }
        }
    }
    return m_action;
}


bool Button::isPressed() const
{
    return m_action == Action::Pressed;
}


bool Button::isReleased() const
{
    return m_action == Action::Released;
}


bool Button::isRepeated() const
{
    return m_action == Action::Repeated;
}


bool Button::isLongPressed() const
{
    return m_action == Action::LongPressed;
}


uint16_t Button::getRepeatCount() const
{
    return m_repeatCount;
}
