#include <Arduino.h>

/**
 * @class Button
 *
 * @brief Keep track of a button's state and handle debounce.
 *
 * The \p read() method returns the last action occurring on the button, in the
 * form of an enumerated value of type \p Button::Action that can be one of
 * \c None, \c Pressed, \c Released or \c Repeated.
 *
 * If the \p read() method is passed a \p repeatDelay value parameter, it will
 * cause it to return the \c Repeated action periodically, as long as the button
 * is kept in its active state. The frequency of the repetition follows the
 * provided delay.
 *
 * The default debounce delay is 50 ms and can be configured manually, either
 * at the class level, or for each button individually. Setting the delay to 0
 * effectively disables debounce handling.
 *
 */
class Button
{
public:
    enum Action { None, Pressed, Released, LongPressed, Repeated };

    enum Option { NoOption, HandleRepeat, HandleLongPress };

    using t_millis       = unsigned long;
    using t_millis_short = uint16_t;

    static uint8_t const Auto, Infinite;
    static t_millis const NoRepeat;

private:
    static t_millis mg_defaultDebounceDelay;

    uint8_t  m_pin              :6;
    bool     m_activeState      :1;
    bool     m_lastState        :1;
    t_millis m_debounceDelay    :16;
    Action   m_action           :3;
    uint16_t m_repeatCount      :13;
    t_millis m_lastStateTime;
    t_millis m_lastRepeatTime;

public:
    /**
     * @brief Set the default debounce delay affecting subsequently created
     * buttons.
     *
     * Each button constructed or configured with \p Button::begin after a
     * call to this class method will have the provided debounce delay.
     *
     * @param delayMs the debounce delay for subsequently created buttons.
     *        Maximum value is 65535 ms.
     */
    static void setDefaultDebounceDelay(t_millis_short delayMs);

    /**
     * @brief Construct a new Button
     *
     * If you use this default constructor you need to call @fn begin to
     * configure the button.
     */
    Button() = default;

    /**
     * @brief Construct a new Button and configure and attach a GPIO pin to the
     * button
     *
     * @param pin the GPIO pin number connected to the button as input
     *
     * @param pinMode the mode in which the pin must be configured:
     *        \c INPUT_PULLUP or \c INPUT (which is the default)
     *
     * @param activeState the logic level associated with a press of the button.
     *        A value of Auto (the default) will determine the active state
     *        according to the @p pinMode parameter
     */
    Button(uint8_t pin, uint8_t pinMode =INPUT, uint8_t activeState =Auto);

    /**
     * @brief Configure and attach a GPIO pin to the button
     *
     * This method needs to be called only if the default constructor was used,
     * or if the configuration of the button's input pin needs to change during
     * the program.
     *
     * @param pin the GPIO pin number connected to the button as input
     *
     * @param pinMode the mode in which the pin must be configured,
     *        \c INPUT_PULLUP or \c INPUT (which is the default)
     *
     * @param activeState the logic level associated with a press of the button.
     *        A value of Auto (the default) will determine the active state
     *        according to the @p pinMode parameter
     */
    void begin(uint8_t pin, uint8_t pinMode =INPUT, uint8_t activeState =Auto);

    /**
     * @brief Retrieve the last action that occurred on the button
     *
     * Should be called only once per button in a \c loop() function iteration.
     *
     * @param option optional parameter of type Button::Option to enable
     *        handling of repeat presses or a single long press
     *
     * @param interval time interval in milliseconds affecting the optional
     *        action provided by the \p option parameter.
     *        This interval should be at least equal to the configured debounce
     *        delay.
     *
     * @return Button::Action a value indicating the last action occurring on
     * the button, if any.
     */
    Action read(Option option = NoOption, t_millis interval = 1000);

    /**
     * @brief Test if the last action on the button was \c Button::Pressed,
     * during the last call to \p read().
     *
     * @return bool
     */
    bool isPressed() const;

    /**
     * @brief Test if the last action on the button was \c Button::Released,
     * during the last call to \p read().
     *
     * @return bool
     */
    bool isReleased() const;

    /**
     * @brief Test if the last action on the button was \c Button::Repeated,
     * during the last call to \p read().
     *
     * @return bool
     */
    bool isRepeated() const;

    /**
     * @brief Test if the last action on the button was \c Button::LongPressed,
     * during the last call to \p read().
     *
     * @return bool
     */
    bool isLongPressed() const;

    /**
     * @brief Set the debounce delay for a button
     *
     * @param delayMs the debounce delay for this button.
     *        Maximum value is 65535 ms.
     */
    void setDebounceDelay(t_millis_short delayMs);

    /**
     * @brief Get the number of times the read method returned Button::Repeated
     * since the last inactive state.
     *
     * The maximum possible value returned by this method is a count of 8191,
     * after which it will roll around to 0.
     *
     * @return uint16_t the repeat count (maximum is 8191)
     */
    uint16_t getRepeatCount() const;
};
