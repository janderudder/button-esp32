/**
 * Button example
 *
 * All in one file for a simulator
 *
 */
class Button
{
public:
    enum Action { None, Pressed, Released, LongPressed, Repeated };
    enum Option { NoOption, HandleRepeat, HandleLongPress };
    using t_millis       = unsigned long;
    using t_millis_short = uint16_t;
    static uint8_t const Auto, Infinite;
    static t_millis const NoRepeat;
private:
    static t_millis mg_defaultDebounceDelay;
    uint8_t  m_pin              :6;
    bool     m_activeState      :1;
    bool     m_lastState        :1;
    t_millis m_debounceDelay    :16;
    Action   m_action           :3;
    uint16_t m_repeatCount      :13;
    t_millis m_lastStateTime;
    t_millis m_lastRepeatTime;
public:
    static void setDefaultDebounceDelay(t_millis_short delayMs);
    Button() = default;
    Button(uint8_t pin, uint8_t pinMode =INPUT, uint8_t activeState =Auto);
    void begin(uint8_t pin, uint8_t pinMode =INPUT, uint8_t activeState =Auto);
    Action read(Option option = NoOption, t_millis interval = 1000);
    bool isPressed() const;
    bool isReleased() const;
    bool isRepeated() const;
    bool isLongPressed() const;
    void setDebounceDelay(t_millis_short delayMs);
    uint16_t getRepeatCount() const;
};

////////////////////////////////////////////////////////////////////////////////
// example program
////////////////////////////////////////////////////////////////////////////////
// GPIO 1, INPUT_PULLUP, active LOW (Auto)
Button button1(1, INPUT_PULLUP);

// GPIO 2, INPUT (default), active HIGH (Auto)
Button button2(2);

// we'll configure this one in the setup for example purposes
Button button3;

void setup()
{
    // configure button 3
    button3.begin(3, INPUT_PULLUP);

    // change the debounce delay for button 1, for example purposes
    button1.setDebounceDelay(80);

    Serial.begin(9600);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    Serial.printf("one button data size: %lu bytes\n", sizeof(Button));
}

void loop()
{
    // Read the state of button 1.
    // This is mandatory to get an up to date answer from the various getters
    // like isPressed(), isReleased() etc.
    button1.read();

    // Let's test if button 1 was pressed or released since the last loop()
    // iteration
    if (button1.isPressed())
    {
        digitalWrite(LED_BUILTIN, HIGH);
        Serial.println("Button 1 pressed");
    }
    else if (button1.isReleased())
    {
        digitalWrite(LED_BUILTIN, LOW);
        Serial.println("Button 1 released");
    }

    // For button 2 we also want to know if it's held down.
    // In that case we want to get a Repeated signal every 1000 milliseconds.
    // Here we're not using the getter methods but testing the action returned
    // by read, it can be done either way.
    switch (button2.read(Button::HandleRepeat, 1000))
    {
        case Button::Pressed:
            Serial.printf("Button 2 pressed at timestamp %lu\n", millis());
            break;

        case Button::Released:
            Serial.println("Button 2 released");
            break;

        case Button::Repeated:
            Serial.printf("Button 2 repeated at timestamp %lu\n", millis());

        default:
            break;
    }

    // On button 3 we only want to react to a single long press of 2 seconds.
    if (button3.read(Button::HandleLongPress, 2000) == Button::LongPressed) {
        Serial.println("Button 3 was pressed for a while");
    }

    // We can mix the ways to interrogate the button, just call read() only
    // once per button and per loop().
    if (button3.isPressed()) {
        Serial.println("Button 3 was just pressed");
    }

    delay(5);
}
////////////////////////////////////////////////////////////////////////////////
// end of example program
////////////////////////////////////////////////////////////////////////////////

static uint8_t _activeStateForPinMode(uint8_t pinMode)
{
    if (pinMode == INPUT_PULLUP) {
        return LOW;
    } else {
        return HIGH;
    }
}

Button::t_millis Button::mg_defaultDebounceDelay = 50;
uint8_t const Button::Auto = 2;
uint8_t const Button::Infinite = 0;
Button::t_millis const Button::NoRepeat = (Button::t_millis)(-1);

void Button::setDefaultDebounceDelay(t_millis_short delayMs)
{
    mg_defaultDebounceDelay = delayMs;
}

Button::Button(uint8_t pin, uint8_t pin_mode, uint8_t activeState)
{
    begin(pin, pin_mode, activeState);
}

void Button::begin(uint8_t pin, uint8_t pin_mode, uint8_t activeState)
{
    m_activeState = (activeState==Button::Auto) ?
        _activeStateForPinMode(pin_mode) : activeState;

    m_pin = pin;
    m_debounceDelay = mg_defaultDebounceDelay;
    m_repeatCount = 0;

    pinMode(m_pin, pin_mode);
    delayMicroseconds(100);
    m_lastState = digitalRead(m_pin);
}

void Button::setDebounceDelay(t_millis_short delayMs)
{
    m_debounceDelay = delayMs;
}

Button::Action Button::read(Option option, t_millis interval)
{
    m_action = None;
    t_millis const currentTime = millis();

    if (currentTime - m_lastStateTime >= m_debounceDelay)
    {
        bool const currentState = digitalRead(m_pin);

        // state change
        if (currentState != m_lastState)
        {
            m_lastState = currentState;
            m_lastStateTime = currentTime;
            m_lastRepeatTime = currentTime;
            if (currentState == m_activeState) {
                m_action = Pressed;
                m_repeatCount = 0;
            } else {
                m_action = Released;
            }
        }
        // state: no change and active
        else if (m_lastState == m_activeState
            && (currentTime - m_lastRepeatTime >= interval))
        {
            m_lastRepeatTime = currentTime;
            m_repeatCount += 1;
            if (option == HandleRepeat) {
                m_action = Repeated;
            }
            else if (option == HandleLongPress && m_repeatCount == 1) {
                m_action = LongPressed;
            }
        }
    }
    return m_action;
}

bool Button::isPressed() const
{
    return m_action == Action::Pressed;
}

bool Button::isReleased() const
{
    return m_action == Action::Released;
}

bool Button::isRepeated() const
{
    return m_action == Action::Repeated;
}

bool Button::isLongPressed() const
{
    return m_action == Action::LongPressed;
}

uint16_t Button::getRepeatCount() const
{
    return m_repeatCount;
}
