#include "Button.hpp"

#include <Arduino.h>


// buttons definitions

// GPIO 1, INPUT_PULLUP, active LOW (Auto)
Button button1(1, INPUT_PULLUP);

// GPIO 2, INPUT (default), active HIGH (Auto)
Button button2(2);

// we'll configure this one in the setup for example purposes
Button button3;


void setup()
{
    // configure button 3
    button3.begin(3, INPUT_PULLUP);

    // change the debounce delay for button 1, for example purposes
    button1.setDebounceDelay(80);

    Serial.begin(9600);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    Serial.printf("one button data size: %lu bytes\n", sizeof(Button));
}


void loop()
{
    // Read the state of button 1.
    // This is mandatory to get an up to date answer from the various getters
    // like isPressed(), isReleased() etc.
    button1.read();

    // Let's test if button 1 was pressed or released since the last loop()
    // iteration
    if (button1.isPressed())
    {
        digitalWrite(LED_BUILTIN, HIGH);
        Serial.println("Button 1 pressed");
    }
    else if (button1.isReleased())
    {
        digitalWrite(LED_BUILTIN, LOW);
        Serial.println("Button 1 released");
    }

    // For button 2 we also want to know if it's held down.
    // In that case we want to get a Repeated signal every 1000 milliseconds.
    // Here we're not using the getter methods but testing the action returned
    // by read, it can be done either way.
    switch (button2.read(Button::HandleRepeat, 1000))
    {
        case Button::Pressed:
            Serial.printf("Button 2 pressed at timestamp %lu\n", millis());
            break;

        case Button::Released:
            Serial.println("Button 2 released");
            break;

        case Button::Repeated:
            Serial.printf("Button 2 repeated at timestamp %lu\n", millis());

        default:
            break;
    }

    // On button 3 we want to react to a single long press of 2 seconds.
    if (button3.read(Button::HandleLongPress, 2000) == Button::LongPressed) {
        Serial.println("Button 3 was pressed for a while");
    }

    // We can mix the ways to interrogate the button, just call read() only
    // once per button and per loop().
    if (button3.isPressed()) {
        Serial.println("Button 3 was just pressed");
    }
}
